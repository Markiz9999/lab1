public class ArrayList<T extends Object> implements List<T> {
    private Object[] elements;
    private int size;
    private int capacity;

    public ArrayList(){
        this(INIT_CAPACITY);
    }

    public ArrayList(int capacity){
        size = 0;
        this.capacity = capacity;
        elements = new Object[capacity];
    }

    public int size() {
        return size;
    }

    public T get(int index) {
        rangeCheck(index);
        return (T) elements[index];
    }

    public void add(T item, int index) {
        rangeCheckForAdd(index);

        if(size + 1 == capacity) {
            capacity = capacity * 2;
            Object[] newObjects = new Object[capacity];
            System.arraycopy(elements, 0, newObjects, 0, size);
            elements = newObjects;
        }

        System.arraycopy(elements, index, elements, index + 1, size-index);
        elements[index] = item;
        size++;
    }

    public void update(T item, int index) {
        rangeCheck(index);
        elements[index] = item;
    }

    public T remove(int index) {
        rangeCheck(index);
        T result = (T) elements[index];
        if(index != size - 1) {
            System.arraycopy(elements, index + 1, elements, index, size - index);
        }
        size--;
        return result;
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < size; i++) {
            result += elements[i] + (i < size - 1 ? " " : "");
        }
        return result;
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= this.size)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private void rangeCheckForAdd(int index) {
        if (index < 0 || index > this.size)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private String outOfBoundsMsg(int index) {
        return "Index: "+index+", Size: "+this.size;
    }
}
