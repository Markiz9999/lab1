public class Main {


    public static void main(String[] args) throws InterruptedException {
        // deadlock example
//        deadLock();

        // singleton example
//        Friend f1 = new Friend("Bohdan");
//        Friend f2 = new Friend("Mike");
//        Friend f3 = new Friend("Alice");
//        Friend f4 = new Friend("Jackson");
//        Friend f5 = new Friend("Optimus");

        // queue demonstration
        MyQueue queue = new MyQueue();
        Consumer consumer = new Consumer(queue);
        consumer.start();
        new Producer(queue);
        Thread.sleep(500);
        consumer.interrupt();
    }

    private static void deadLock() {
        final Friend alphonse =
                new Friend("Alphonse");
        final Friend gaston =
                new Friend("Gaston");
        new Thread(new Runnable() {
            @Override
            public void run() {
                // System.out.println("Thread 1");
                alphonse.bow(gaston);
                // System.out.println("Th: gaston bowed to alphonse");
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                //  System.out.println("Thread 2");
                gaston.bow(alphonse);
                //  System.out.println("2.gaston waiting alph bowed");
            }
        }).start();
    }
}
