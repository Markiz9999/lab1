import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Main {

    static class City{
        private String state;
        private long population;

        City(String state, long population) {
            this.state = state;
            this.population = population;
        }

        public String getState() {
            return state;
        }

        public long getPopulation() {
            return population;
        }
    }

    public static Map<String, City> getLargestCityPerState(Collection<City> cities) {
        return cities.stream().collect(
            Collectors.toMap(
                City::getState,
                Function.identity(),
                BinaryOperator.maxBy(Comparator.comparing(City::getPopulation))
            )
        );
    }

    public static String intListToString(List<Integer> list){
        return list.stream()
            .map(integer -> (integer % 2 == 0 ? 'e' : 'o') + integer.toString())
            .reduce((s, s2) -> s+','+s2).get();
    }

    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Queue<T> elementsA = first.collect(Collectors.toCollection(LinkedList::new));
        Queue<T> elementsB = second.limit(elementsA.size()).collect(Collectors.toCollection(LinkedList::new));
        return Stream.generate(new Supplier<T>() {
            boolean first = true;

            @Override
            public T get() {
                Queue<T> queue = first ? elementsA : elementsB;
                first = !first;
                return queue.poll();
            }
        }).limit(Math.min(elementsA.size(), elementsB.size()) * 2);
    }

    public static void main(String[] args) {

        ///////////////////////////////////////////////////////////
        // 1
        List<Integer> list = new ArrayList<>();
        for (int i = -20; i <= 20; i++) {
            list.add(i);
        }
        System.out.println(intListToString(list));

        ///////////////////////////////////////////////////////////
        // 2
        List<City> list1 = new ArrayList<>();
        list1.add(new City("A", 1132));
        list1.add(new City("B", 18648));
        list1.add(new City("C", 7891));
        list1.add(new City("A", 34100));
        list1.add(new City("A", 99840)); // largest
        list1.add(new City("V", 89781)); // largest
        list1.add(new City("C", 45632));
        list1.add(new City("D", 798123)); // largest
        list1.add(new City("C", 489432)); // largest
        list1.add(new City("B", 46823)); // largest
        list1.add(new City("B", 33248));

        Map<String, City> largestCities = getLargestCityPerState(list1);
        System.out.println("Largest cities:\n");
        largestCities.forEach((key, value) -> System.out.println("State: " + value.state + ", Population: " + value.population));

        ///////////////////////////////////////////////////////////
        // 3
        Stream<Integer> odd = IntStream.iterate(1, x -> x + 2).limit(10).boxed();
        Stream<Integer> even = IntStream.iterate(2, x -> x + 2).limit(13).boxed();
        List<Integer> elements = zip(odd, even).collect(toList());
        System.out.println(intListToString(elements));
    }
}
