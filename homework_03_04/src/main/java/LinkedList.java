public class LinkedList<T extends Object> implements List<T> {
    public class Node{
        private T data;
        private Node next;

        public Node(T data) {
            this(data, null);
        }

        public Node(T data, Node next) {
            this.data = data;
            this.next = next;
        }

        public T getData() {
            return data;
        }

        public Node getNext() {
            return next;
        }

        public void setData(T data) {
            this.data = data;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    Node head = null;
    int size = 0;

    public void push(T item){
        add(item,size);
    }

    public T pop(){
        return remove(size-1);
    }

    public void add(T item, int index){
        rangeCheckForAdd(index);

        Node inserted = new Node(item);

        if (head == null || index == 0){
            Node old = head;
            head = inserted;
            inserted.setNext(old);
        }
        else {
            Node prev = getNode(index-1);
            Node next = prev.getNext();
            if(next != null){
                prev.setNext(inserted);
                inserted.setNext(next);
            }else{
                prev.setNext(inserted);
            }
        }
        size++;
    }

    public void update(T item, int index) {
        rangeCheck(index);
        getNode(index).setData(item);
    }

    private Node getNode(int index) {
        rangeCheck(index);

        Node temp = head;
        for (int i = 0; i < index; i++) {
            temp = temp.next;
        }
        return temp;
    }

    public T remove(int index) {
        rangeCheck(index);

        Node deleted;

        if(index == 0){
            deleted = head;
            head = head.getNext();
        }
        else{
            Node prev = getNode(index-1);
            deleted = prev.getNext();
            prev.setNext(deleted.getNext());
        }
        size--;
        return deleted.getData();
    }

    public T get(int index) {
        return getNode(index).getData();
    }

    public int size() {
        return size;
    }

    @Override
    public String toString() {
        String result = "";
        for (Node node = head; node != null; node = node.getNext()) {
            result += node.getData() + (node.next != null ? " " : "");
        }
        return result;
    }

    private void rangeCheck(int index) {
        if (index < 0 || index >= this.size)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private void rangeCheckForAdd(int index) {
        if (index < 0 || index > this.size)
            throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
    }

    private String outOfBoundsMsg(int index) {
        return "Index: "+index+", Size: "+this.size;
    }
}
