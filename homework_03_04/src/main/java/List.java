public interface List <T extends Object> {
    int INIT_CAPACITY = 10;
    int size();
    T get(int index);

    void add(T item, int index);
    void update(T item, int index);
    T remove(int index);
}
