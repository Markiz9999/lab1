import java.util.stream.Collectors;

public class Friend {
    private final String name;
    public Friend(String name) {
        this.name = name;

        Singleton singleton = Singleton.getInstance();

        singleton.addFriend(name);

        System.out.println("Singleton has the next friends:");
        System.out.println(singleton.getFriends().stream().collect(Collectors.joining(", ")));
    }
    public String getName() {
        return this.name;
    }
    public synchronized void bow(Friend bower) {
        System.out.format("%s: %s" + "  has bowed to me!%n", this.name, bower.getName());
        bower.bowBack(this);
    }
    public synchronized void bowBack(Friend bower) {
        System.out.format("%s: %s"
                        + " has bowed back to me!%n",
                this.name, bower.getName());
    }
}
