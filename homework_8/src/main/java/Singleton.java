import java.util.LinkedList;
import java.util.List;

public class Singleton {
    private static volatile Singleton instance;

    public static Singleton getInstance() {
        Singleton localInstance = instance;
        if (localInstance == null) {
            synchronized (Singleton.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Singleton();
                }
            }
        }
        return localInstance;
    }

    private List<String> namesOfFriends = new LinkedList<String>();

    public void addFriend(String name) {
        namesOfFriends.add(name);
    }

    public List<String> getFriends() {
        return  namesOfFriends;
    }
}
