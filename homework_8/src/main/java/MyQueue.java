import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class MyQueue {
    private int value;
    private boolean valueSet = false;
    private ReentrantLock lock = new ReentrantLock();
    private Condition queueEmpty = lock.newCondition();
    private Condition queueFull = lock.newCondition();

    public void put(int value) {
        try {
            lock.lockInterruptibly();
            while (valueSet) {
                queueFull.await();
            }

            this.value = value;
            valueSet = true;

            queueEmpty.signalAll();
        } catch (InterruptedException e) {
            System.out.println("Put interruption was handled");
        } finally {
            lock.unlock();
        }

        System.out.println("Sent: " + value);
    }

    public int get() {
        try {
            lock.lockInterruptibly();

            while (!valueSet) {
                queueEmpty.await();
            }

            valueSet = false;
            System.out.println("Received: " + value);

            return value;
        } catch (InterruptedException e) {
            System.out.println("Get interruption was handled");
            Thread.currentThread().interrupt();
            return 0;
        } finally {
            queueFull.signalAll();
            lock.unlock();
        }
    }
}
