import org.junit.Assert;
import org.junit.Test;

public class LinkedListTest {
    @Test
    public void ListAddTest() {
        // GIVEN
        int expectedLength = 1;
        int expectedItem = 15;

        // WHEN
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(expectedItem, 0);

        int actualLength = list.size();
        int actualItem = list.get(0);

        // THEN
        Assert.assertEquals(expectedLength, actualLength);
        Assert.assertEquals(expectedItem, actualItem);
    }

    @Test
    public void ListRemoveTest() {
        // GIVEN
        int expectedLength = 0;
        int expectedItem = 15;

        // WHEN
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(expectedItem, 0);

        int actualItem = list.remove(0);
        int actualLength = list.size();

        // THEN
        Assert.assertEquals(expectedLength, actualLength);
        Assert.assertEquals(expectedItem, actualItem);
    }

    @Test
    public void ListUpdateTest() {
        // GIVEN
        int expectedLength = 1;
        int expectedItem = 15;

        // WHEN
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add((int)(Math.random() * 1000), 0);
        list.update(expectedItem, 0);

        int actualLength = list.size();
        int actualItem = list.get(0);

        // THEN
        Assert.assertEquals(expectedLength, actualLength);
        Assert.assertEquals(expectedItem, actualItem);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void  ListIndexBoundaryTest() {
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add((int)(Math.random() * 1000), 0);
        list.get(1);
    }
}