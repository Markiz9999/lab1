public class Main {
    public static void main(String[] args){
        LinkedList<Integer> list = new LinkedList<Integer>();
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            list.push(i);
            list1.add(i,i);
        }
        list.remove(0);
        list.remove(8);
        list.add(90,8);
        list.add(91,0);
        list.update(500,4);
        System.out.println("Size: " + list.size());
        System.out.println(list);

        System.out.println();

        list1.remove(0);
        list1.remove(8);
        list1.add(94,8);
        list1.add(95,0);
        list1.update(500,4);
        System.out.println("Size: " + list1.size());
        System.out.println(list1);
    }
}
